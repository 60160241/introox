package org.kotlinlang.play
import java.util.*

object XO {
    var XO = arrayOf(
            arrayOf(" ", " ", " "),
            arrayOf(" ", " ", " "),
            arrayOf(" ", " ", " ")
    )
    var turn = "X"
    var Row = 0
    var Column = 0
    var Round = 0

    @JvmStatic
    fun main(args: Array<String>) {
        printStart()
        while (true) {
            printBoard()

            inputPosition()

            if (checkWin()) {
                break
            } else if (checkDraw()) {
                break
            }

            switchTurn()
        }
    }

    fun printStart() {
        println("Welcome to Game XO")
    }

    fun printBoard() {
        println(" " + " 1 " + "2" + " 3 ")
        for (i in 0..2) {
            print(i + 1)
            for (j in 0..2) {
                print("|" + XO[i][j])
            }
            print("|")
            println()
        }
    }

    fun inputPosition() {
        val kb = Scanner(System.`in`)
        while (true) {
            println("Turn $turn")
            print("Please Choose Position(Row,Column) : ")
            try {
                val a = kb.next()
                val b = kb.next()
                Row = a.toInt()
                Column = b.toInt()
                if (Row > 3 || Row < 0 || Row == 0 || Column > 3 || Column < 0 || Column == 0) {
                    println("Row and Column must be number 1-3")
                    continue
                }
                Row = Row - 1
                Column = Column - 1
                if (XO[Row][Column] !== " ") {
                    println("Row " + (Row + 1) + " and Column " + (Column + 1) + " can't choose again")
                    continue
                }
                Round++
                XO[Row][Column] = turn
                break
            } catch (a: Exception) {
                println("Row and Column must be number")
                continue
            }
        }
    }

    fun checkWin(): Boolean {
        var check = false
        for (i in XO.indices) {
            if (XO[i][0] === turn && XO[i][1] === turn && XO[i][2] === turn
            ) {
                check = true
            }
        }
        for (i in XO.indices) {
            if (XO[0][i] === turn && XO[1][i] === turn && XO[2][i] === turn
            ) {
                check = true
            }
        }
        if (XO[0][0] === turn && XO[1][1] === turn && XO[2][2] === turn
        ) {
            check = true
        }
        if (XO[0][2] === turn && XO[1][1] === turn && XO[2][0] === turn
        ) {
            check = true
        }
        if (check == true) {
            printBoard()
            println("$turn WIN")
            return true
        }
        return false
    }

    fun checkDraw(): Boolean {
        if (Round == 9) {
            printBoard()
            println("DRAW")
            return true
        }
        return false
    }

    fun switchTurn() {
        turn = if (turn === "X") {
            "O"
        } else {
            "X"
        }
    }
}